# dwarves

#### 介绍
调试信息操作工具

#### 软件架构
支持aarch64/x86_64两种架构


#### 安装教程
rpm -ivh dwarves***.rpm

#### 使用说明
使用方式查看man手册

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request